package com.omega323.hajduk_brojclanova;

import android.os.AsyncTask;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class News extends AppCompatActivity{

    TextView tvNewsText;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.news);

        Log.d("NewsLOG","activity started");

        String title= getIntent().getStringExtra("title");
        String link = getIntent().getStringExtra("link");
        String image = getIntent().getStringExtra("image");

        Log.d("NewsLOG", "title= " + title);

        TextView tvNewsTitle = (TextView) findViewById(R.id.newsTitle);
        tvNewsTitle.setText(title);

        ImageView newsImageView = (ImageView) findViewById(R.id.imageView);
        Picasso.with(this).load(image).into(newsImageView);


        tvNewsText = (TextView) findViewById(R.id.newsText);
        new getNews().execute(link);
    }


    public class getNews extends AsyncTask<String,Integer,String>{

        String description = "";

        @Override
        protected String doInBackground(String... params) {

            try{
            Document news = Jsoup.connect(params[0]).get();
            Elements par = news.select("p");
            int size = par.size();
            int count=0;
            for(Element text : par){
                if(count>0 && count<size-1) {
                    description = description+ System.getProperty("line.separator") + text.text();
                }
                count++;
            }
        }
            catch (Exception e){

            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            tvNewsText.setText(description);
        }
    }
}
