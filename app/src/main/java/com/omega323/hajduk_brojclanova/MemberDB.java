package com.omega323.hajduk_brojclanova;

public class MemberDB {

    int id;
    String date;
    int number;

    public MemberDB(){
    }

    public MemberDB(int id,String date, int number){
        this.id = id;
        this.date = date;
        this.number = number;
    }

    public MemberDB(String date, int number){
        this.date = date;
        this.number = number;
    }

    public int getID(){
        return this.id;
    }

    public void setID(int id){
        this.id = id;
    }

    public String getDate(){
        return this.date;
    }

    public void setDate(String date){
        this.date = date;
    }

    public int getNumber(){
        return this.number;
    }

    public void setNumber(int number){
        this.number = number;
    }
}
