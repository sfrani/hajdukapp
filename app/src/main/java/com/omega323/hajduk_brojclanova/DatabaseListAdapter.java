package com.omega323.hajduk_brojclanova;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

public class DatabaseListAdapter extends ArrayAdapter<String> {

    Context context;
    ArrayList<String> dates;
    ArrayList<String> numbers;
    ViewHolder holder;
    View rowView;

    public DatabaseListAdapter (Context context, ArrayList<String> dates, ArrayList<String> numbers) {
        super(context, R.layout.db_list, dates);
        // TODO Auto-generated constructor stub

        this.context=context;
        this.dates = new ArrayList<>(dates);
        this.numbers = new ArrayList<>(numbers);
    }

    public View getView(int position,View view,ViewGroup parent) {
        rowView = view;

        if(rowView==null){
            LayoutInflater inflater = (LayoutInflater) context.getSystemService( Context.LAYOUT_INFLATER_SERVICE );
            rowView=inflater.inflate(R.layout.db_list_row, null,true);
            holder = new ViewHolder();

            holder.txtDate = (TextView) rowView.findViewById(R.id.tvDate);
            holder.txtNumber = (TextView) rowView.findViewById(R.id.tvNumber);

            rowView.setTag(holder);
        }
        else{
            holder = (ViewHolder) rowView.getTag();
        }

        holder.txtDate.setText(dates.get(position));
        holder.txtNumber.setText(numbers.get(position));
        return rowView;

    }

    static class ViewHolder {
        TextView txtDate;
        TextView txtNumber;
    }
}
