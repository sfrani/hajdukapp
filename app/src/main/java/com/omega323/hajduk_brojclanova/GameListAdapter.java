package com.omega323.hajduk_brojclanova;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class GameListAdapter extends ArrayAdapter<String> {

    Context context;
    ArrayList<String> match;
    ArrayList<String> date;
    ArrayList<String> homeLogo;
    ArrayList<String> awayLogo;

    public GameListAdapter(Context context, ArrayList<String> match, ArrayList<String> date, ArrayList<String> homeLogo, ArrayList<String> awayLogo) {
        super(context, R.layout.mylist, match);
        // TODO Auto-generated constructor stub

        this.context = context;
        this.match = new ArrayList<>(match);
        this.date = new ArrayList<>(date);
        this.homeLogo = new ArrayList<>(homeLogo);
        this.awayLogo = new ArrayList<>(awayLogo);
    }

    public View getView(int position, View view, ViewGroup parent) {
        // LayoutInflater inflater=context.getLayoutInflater();
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView = inflater.inflate(R.layout.schedule, null, true);

        TextView game = (TextView) rowView.findViewById(R.id.match);
        ImageView homeTeam = (ImageView) rowView.findViewById(R.id.iconHome);
        ImageView awayTeam = (ImageView) rowView.findViewById(R.id.iconAway);
        TextView time = (TextView) rowView.findViewById(R.id.time);
        time.setText(date.get(position));
        game.setText(match.get(position));
        Picasso.with(context).load(homeLogo.get(position)).into(homeTeam);
        Picasso.with(context).load(awayLogo.get(position)).into(awayTeam);
        return rowView;

    }

    ;
}
