package com.omega323.hajduk_brojclanova;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

public class CustomListAdapter extends ArrayAdapter<String> {

    Context context;
  //  private final String[] itemname;
   // private final Integer[] imgid;
    ArrayList<String> titles;
    ArrayList<String> descriptions;
    ArrayList<String> images;
    ViewHolder holder;
    View rowView;

    public CustomListAdapter(Context context, ArrayList<String> titles, ArrayList<String> descriptions, ArrayList<String> images) {
        super(context, R.layout.mylist, titles);
        // TODO Auto-generated constructor stub

        this.context=context;
        this.titles = new ArrayList<>(titles);
        this.descriptions = new ArrayList<>(descriptions);
        this.images = new ArrayList<>(images);
    }

    public View getView(int position,View view,ViewGroup parent) {
        rowView = view;
       // LayoutInflater inflater=context.getLayoutInflater();
      /*  LayoutInflater inflater = (LayoutInflater) context.getSystemService( Context.LAYOUT_INFLATER_SERVICE );
        View rowView=inflater.inflate(R.layout.mylist, null,true);

        TextView txtTitle = (TextView) rowView.findViewById(R.id.item);
        ImageView imageView = (ImageView) rowView.findViewById(R.id.icon);
        TextView extratxt = (TextView) rowView.findViewById(R.id.textView1); */

        if(rowView==null){
            LayoutInflater inflater = (LayoutInflater) context.getSystemService( Context.LAYOUT_INFLATER_SERVICE );
            rowView=inflater.inflate(R.layout.mylist, null,true);
            holder = new ViewHolder();

            holder.txtTitle = (TextView) rowView.findViewById(R.id.item);
            holder.imageView = (ImageView) rowView.findViewById(R.id.icon);
            holder.extratxt = (TextView) rowView.findViewById(R.id.textView1);

            rowView.setTag(holder);
        }
        else{
            holder = (ViewHolder) rowView.getTag();
        }

        holder.txtTitle.setText(titles.get(position));
       // imageView.setImageResource(imgid[position]);
        holder.extratxt.setText(descriptions.get(position));
        Picasso.with(context).load(images.get(position)).into(holder.imageView);
        return rowView;

    }

   static class ViewHolder {
        TextView txtTitle;
        ImageView imageView;
        TextView extratxt;
    }
}