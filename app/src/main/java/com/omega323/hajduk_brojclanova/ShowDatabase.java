package com.omega323.hajduk_brojclanova;


import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

public class ShowDatabase extends AppCompatActivity{

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.db_list);

        Log.d("ListLOG", "running");

        ArrayList<String> dates = new ArrayList<>();
        ArrayList<String> numbers = new ArrayList<>();

        DatabaseHandler db = new DatabaseHandler(this);
        List<MemberDB> memberDB = db.getAllData();
        for(MemberDB mdb : memberDB){
            dates.add(mdb.getDate());
            numbers.add(Integer.toString(mdb.getNumber()));
            Log.d("ListLOG", mdb.getDate());
            Log.d("ListLOG",Integer.toString(mdb.getNumber()));
        }
        ListView lv = (ListView) findViewById(R.id.listView2);
        DatabaseListAdapter adapter = new DatabaseListAdapter(this,dates,numbers);
        lv.setAdapter(adapter);

    }
}
