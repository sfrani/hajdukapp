package com.omega323.hajduk_brojclanova;


import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.util.ArrayList;
import java.util.HashMap;


public class Utakmice extends Fragment {

    ListView lv;
    HashMap<String, String> clubMap=new HashMap<String, String>();

    public Utakmice() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        clubMap.put("Dinamo (Z)", "http://prvahnl.hr/static/images/clubs/sq/609.png");
        clubMap.put("Hajduk","http://prvahnl.hr/static/images/clubs/sq/515.png");
        clubMap.put("Istra","http://prvahnl.hr/static/images/clubs/sq/1534.png");
        clubMap.put("Osijek","http://prvahnl.hr/static/images/clubs/sq/150.png");
        clubMap.put("Split","http://prvahnl.hr/static/images/clubs/sq/543.png");
        clubMap.put("Rijeka","http://prvahnl.hr/static/images/clubs/sq/1471.png");
        clubMap.put("Lokomotiva","http://prvahnl.hr/static/images/clubs/sq/594.png");
        clubMap.put("Cibalia","http://prvahnl.hr/static/images/clubs/sq/40242.png");
        clubMap.put("Slaven","http://prvahnl.hr/static/images/clubs/sq/914.png");
        clubMap.put("Inter","http://prvahnl.hr/static/images/clubs/sq/1229.png");
        new GetMatches().execute();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.schedule_list, container, false);
        lv = (ListView) view.findViewById(R.id.listView);
        return view;
    }


    class GetMatches extends AsyncTask<String, Integer, String> {

        ArrayList<String> opponent = new ArrayList<>();
        ArrayList<String> homeaway = new ArrayList<>();
        ArrayList<String> date = new ArrayList<>();
        ArrayList<String> homeLogo = new ArrayList<>();
        ArrayList<String> awayLogo = new ArrayList<>();
        ArrayList<String> match = new ArrayList<>();

        @Override
        protected String doInBackground(String... params) {
            try {
                Log.d("GameLOG","running");
                Document doc = Jsoup.connect("http://hajduk.hr").get();
                Elements elements = doc.select("section");
                for(Element mat : elements){
                    if(mat.attr("class").startsWith("utakmice")){
                        Log.d("GameLOG","utakmice");
                          // Log.d("GameLOG", mat.getElementsByTag("td").text());
                        Elements matches = mat.getElementsByTag("td");
                        for(Element game : matches){
                            if(game.attr("class").startsWith("kl")){
                                Log.d("GameLOG","protivnik:" + game.text());
                                opponent.add(game.text());
                            }
                            if(game.attr("class").startsWith("br")){
                                Log.d("GameLOG","termin:" + game.text());
                                date.add(game.text());
                            }
                            if(game.attr("class").startsWith("")){
                                String place = game.text();
                                if(place.length()<2){
                                    homeaway.add(place);
                                Log.d("GameLOG","mjesto:" + place); }
                            }
                        }
                    }
                }
          /*      int count=0;
                Elements elements = doc.select("td");
                for(Element mat : elements) {
                    if (mat.attr("class").startsWith("kl")) {
                        Log.d("GameLOG", "protivnik:" + mat.text());
                        opponent.add(mat.text());
                    } else if (mat.attr("class").startsWith("br")) {
                        Log.d("GameLOG", "termin:" + mat.text());
                        //  opponent.add(mat.text());
                    }
                } */


            } catch (Exception e) {
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            opponent.remove(0);
            date.remove(0);
            homeaway.remove(0);
            homeaway.remove(0);
            for(int i=0;i<homeaway.size();i++){
                if(homeaway.get(i).equals("D")){
                    homeLogo.add(clubMap.get("Hajduk"));
                    awayLogo.add(clubMap.get(opponent.get(i)));
                    match.add("Hajduk - " + opponent.get(i));
                }
                else{
                    awayLogo.add(clubMap.get("Hajduk"));
                    homeLogo.add(clubMap.get(opponent.get(i)));
                    match.add(opponent.get(i) + " - Hajduk");
                }
            }
            GameListAdapter adapter = new GameListAdapter(getActivity(),match,date,homeLogo,awayLogo);
            lv.setAdapter(adapter);
        }
    }
}
