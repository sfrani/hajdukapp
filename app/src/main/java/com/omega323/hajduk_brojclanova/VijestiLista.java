package com.omega323.hajduk_brojclanova;

import android.animation.ValueAnimator;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

public class VijestiLista extends Fragment {

    private TextView textView;
    private ListView lv;
    private SwipeRefreshLayout swipeLayout;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        swipeLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                new GetData().execute();
            }
        });

        new GetData().execute();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
      //  return inflater.inflate(R.layout.activity_home, container, false);
         inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.activity_home, container, false);
        lv = (ListView) view.findViewById(R.id.listView);
        swipeLayout = (SwipeRefreshLayout) view.findViewById(R.id.swipe_container);
        return view;
    }

    class GetData extends AsyncTask<String, Void, String> {

        ArrayList<String> titles = new ArrayList<>();
        ArrayList<String> descriptions = new ArrayList<>();
        ArrayList<String> images = new ArrayList<>();
        ArrayList<String> links = new ArrayList<>();

        @Override
        protected String doInBackground(String... params) {

            HttpURLConnection urlConnection = null;
            String result = "";
            try {
                URL url = new URL("http://hajduk.hr/klub/clanstvo");
                urlConnection = (HttpURLConnection) url.openConnection();


                Document doc = Jsoup.connect("http://hajduk.hr/vijesti").get();
                Log.d("JLOG", doc.title());
              /*  Elements titles = doc.select("h1");
                for(Element tit : titles){
                  //  Log.d("JLOG",tit.text());
                    Log.d("JLOG",tit.parent().text());
                } */

                Elements newstitles = doc.select("div");
                for(Element tit : newstitles){
                    //  Log.d("JLOG",tit.text());
                    if(tit.attr("class").startsWith("n")) {
                        Log.d("JLOG",tit.getElementsByTag("h1").text());
                        //  Log.d("JLOG",tit.getElementsByTag("h3").text());
                        //  String title = tit.getElementsByTag("h1").text();
                        Log.d("AdapterDataLOG", "TITLES: " + tit.getElementsByTag("h1").text());
                        titles.add(tit.getElementsByTag("h1").text());
                        String description = tit.getElementsByTag("h3").text();
                        description = description.substring(0,description.length()-6);
                        Log.d("AdapterDataLOG","DESCRIPTIONS: " + description);
                        descriptions.add(description);
                        //  Log.d("JLOG",description.substring(0,description.length()-6));
                        //  Log.d("JLOG",tit.getElementsByTag("em").text());
                        // Log.d("JLOG", tit.text());
                    }
                }

             /*   Elements links = doc.select("a");
                for(Element el : links){
                    if(el.attr("href").startsWith("vijest/")){
                         Log.d("JLOG","NASLOV: " + el.attr("href"));
                        String newsURL = "http://hajduk.hr/" + el.attr("href");
                        Document news = Jsoup.connect(newsURL).get();
                        Elements par = news.select("p");
                        int size = par.size();
                        int count=0;
                        for(Element text : par){
                            if(count>0 && count<size-1) {
                                Log.d("JLOG", text.text());
                            }
                            count++;
                        }
                    }
                }   */

                Elements newslinks = doc.select("a");
                for(Element el : newslinks) {
                    if (el.attr("href").startsWith("vijest/")) {
                        Log.d("JLOG", "NASLOV: " + el.attr("href"));
                        String newsURL = "http://hajduk.hr/" + el.attr("href");
                        links.add(newsURL);
                    }
                }

                Elements newsimages = doc.select("img");
                for(Element im : newsimages){
                    if(im.attr("src").startsWith("http://hajduk.hr/sadrzaj/slike-za-vijesti/")){
                        Log.d("JLOG", im.attr("src"));
                        String myImage = im.attr("src");
                        myImage = myImage.replace("200x100","800x400");
                       // images.add(im.attr("src"));
                        images.add(myImage);
                    }
                }

              /*  Document news = Jsoup.connect("http://hajduk.hr/vijest/igraci-i-strucni-stozer-na-biloj-noci-u-steyru/7368").get();
                Elements par = news.select("p");
                int size = par.size();
                int count=0;
                for(Element text : par){
                    if(count>0 && count<size-1) {
                        Log.d("JLOG", text.text());
                    }
                    count++;
                } */
              /*  Elements metaElems = doc.select("div");
                for (Element metaElem : metaElems) {
                    String name = metaElem.attr("s");
                    Log.d("HTML",name);
                }  */

               /*  Document matches = Jsoup.connect("http://hajduk.hr/utakmice").get();
                Elements gameList = matches.select("div");
                for(Element gm : gameList){
                    if(gm.attr("class").equals("box_4")){
                    Log.d("GamesLOG",gm.text()); }
                } */

             /*   int code = urlConnection.getResponseCode();

                if (code == 200) {
                    InputStream in = new BufferedInputStream(urlConnection.getInputStream());
                    if (in != null) {
                        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(in));
                        String line = "";
                        int i=1;
                        while ((line = bufferedReader.readLine()) != null) {
                           if(i==245){ result += line;}
                            i++;
                        }
                    }
                    in.close();
                }

                return result;  */
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                //   urlConnection.disconnect();
            }
            return result;

        }

        @Override
        protected void onPostExecute(String result) {
          /*  String brojClanova = result.substring(0, result.lastIndexOf(","));
            brojClanova = brojClanova.substring(0,brojClanova.lastIndexOf(","));
            brojClanova = brojClanova.substring(brojClanova.lastIndexOf(",")+2,brojClanova.length());
            int broj_clanova = Integer.parseInt(brojClanova);
            animateTextView(0, broj_clanova, textView);  */
            CustomListAdapter adapter = new CustomListAdapter(getActivity(), titles, descriptions, images);
            lv.setAdapter(adapter);

            swipeLayout.setRefreshing(false);

            lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    Intent i = new Intent(getActivity(), News.class);
                    i.putExtra("title", titles.get(position));
                    i.putExtra("link", links.get(position));
                    i.putExtra("image", images.get(position));
                    startActivity(i);
                }
            });
        }

    }

    public void animateTextView(int initialValue, int finalValue, final TextView textview) {

        ValueAnimator valueAnimator = ValueAnimator.ofInt(initialValue, finalValue);
        valueAnimator.setDuration(3000);

        valueAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator valueAnimator) {

                //  textview.setText("Broj clanova: " + valueAnimator.getAnimatedValue().toString());
                String text = "<font color='red'>BROJ</font><font color='red'> CLANOVA: </font><font color='blue'>" + valueAnimator.getAnimatedValue().toString() + "</font>";
                textView.setText(Html.fromHtml(text), TextView.BufferType.SPANNABLE);
            }
        });
        valueAnimator.start();

    }

}
