package com.omega323.hajduk_brojclanova;


import android.animation.ValueAnimator;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class Clanovi extends Fragment {

    TextView tv;
    TextView tvDiff;
    ImageView iv;
    Button btnSave;
    SharedPreferences settings;
    int broj_clanova;
    public static boolean been = false;


    public Clanovi(){
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if(been) {
            new GetNumber().execute();
        }

        DatabaseHandler db = new DatabaseHandler(getActivity());
        List<MemberDB> memberDB = db.getAllData();
        for(MemberDB mdb : memberDB){
            Log.d("TabLOG",mdb.getID() + " " + mdb.getDate() + " " + mdb.getNumber());
        }

        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy. HH:mm:ss", Locale.ENGLISH);
                String currentTime = sdf.format(new Date());
                Log.d("TabLOG",currentTime);
                DatabaseHandler db = new DatabaseHandler(getActivity());
                db.addData(new MemberDB(currentTime, broj_clanova));
            }
        });
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if(isVisibleToUser && !been){
            new GetNumber().execute();
            been = true;
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.clanovi_fragment, container, false);
        tv = (TextView) view.findViewById(R.id.textView01);
        tvDiff = (TextView) view.findViewById(R.id.textViewDiff);
        iv = (ImageView) view.findViewById(R.id.imageView);
        btnSave = (Button) view.findViewById(R.id.button);
        return view;
    }


    class GetNumber extends AsyncTask<String,Integer,String>{

        String text;

        @Override
        protected String doInBackground(String... params) {
            Log.d("TabLOG","starting...");
            HttpURLConnection urlConnection = null;
            String result = "";
            try {

                Document doc = Jsoup.connect("http://hajduk.hr/klub/clanstvo").get();
                Elements elements = doc.select("div");
                for(Element div : elements){
                    if(div.attr("class").startsWith("box_4 tekst")){
                        text = div.text();
                    }
                }

                URL url = new URL("http://hajduk.hr/klub/clanstvo");
                urlConnection = (HttpURLConnection) url.openConnection();
                int code = urlConnection.getResponseCode();

                if (code == 200) {
                    InputStream in = new BufferedInputStream(urlConnection.getInputStream());
                    if (in != null) {
                        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(in));
                        String line = "";
                        int i=1;
                        while ((line = bufferedReader.readLine()) != null) {
                         //   if(i==245){ result += line;}
                            if(i==415){ result += line;}

                            i++;
                        }
                    }
                    in.close();
                }

                //   return result;
            }
            catch(Exception e){
            }

            return result;
        }


        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            Picasso.with(getActivity()).load("http://hajduk.hr/sadrzaj/klub/clanstvo/clanstvo_1220.jpg").fit().into(iv);
            String brojClanova = result.substring(0, result.lastIndexOf(","));
            brojClanova = brojClanova.substring(0,brojClanova.lastIndexOf(","));
            brojClanova = brojClanova.substring(brojClanova.lastIndexOf(",")+2,brojClanova.length());
            broj_clanova = Integer.parseInt(brojClanova);
            animateTextView(0, broj_clanova, tv);

            settings = PreferenceManager.getDefaultSharedPreferences(getActivity());
            int lastTime = settings.getInt("Broj_clanova", 0);
            if(lastTime !=0){
                int diff = broj_clanova - lastTime;
                tvDiff.setText("Od zadnje posjete aplikaciji uclanilo se " + diff + " osoba");
            }

            settings.edit().putInt("Broj_clanova",broj_clanova).apply();

            //  DatabaseHandler db = new DatabaseHandler(getActivity());
            //  db.addData(new MemberDB(currentTime,broj_clanova));

        }
    }

    public void animateTextView(int initialValue, int finalValue, final TextView textview) {

        ValueAnimator valueAnimator = ValueAnimator.ofInt(initialValue, finalValue);
        valueAnimator.setDuration(3000);

        valueAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator valueAnimator) {

                //  textview.setText("Broj clanova: " + valueAnimator.getAnimatedValue().toString());
                String text = "<font color='red'>BROJ</font><font color='red'> CLANOVA: </font><font color='blue'>" + valueAnimator.getAnimatedValue().toString() + "</font>";
                tv.setText(Html.fromHtml(text), TextView.BufferType.SPANNABLE);
            }
        });
        valueAnimator.start();

    }
}