package com.omega323.hajduk_brojclanova;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.List;

public class DatabaseHandler extends SQLiteOpenHelper {

    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "myDatabase";
    private static final String TABLE_CLANOVI = "clanovi";

    private static final String KEY_ID = "id";
    private static final String KEY_DATE = "date";
    private static final String KEY_NUMBER = "number";

    public DatabaseHandler(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String CREATE_CLANOVI_TABLE = "CREATE TABLE " + TABLE_CLANOVI + "("
                + KEY_ID + " INTEGER PRIMARY KEY," + KEY_DATE + " TEXT,"
                + KEY_NUMBER + " INTEGER" + ")";
        db.execSQL(CREATE_CLANOVI_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    void addData(MemberDB member) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_DATE, member.getDate());
        values.put(KEY_NUMBER, member.getNumber());
        db.insert(TABLE_CLANOVI, null, values);
        db.close();
    }

    public MemberDB getData(int id) {
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.query(TABLE_CLANOVI, new String[] { KEY_ID,
                        KEY_DATE, KEY_NUMBER }, KEY_ID + "=?",
                new String[] { String.valueOf(id) }, null, null, null, null);
        if (cursor != null)
            cursor.moveToFirst();

        MemberDB member = new MemberDB(Integer.parseInt(cursor.getString(0)),
                cursor.getString(1), cursor.getInt(2));
        return member;
    }

    public List<MemberDB> getAllData() {
        List<MemberDB> contactList = new ArrayList<>();
        String selectQuery = "SELECT  * FROM " + TABLE_CLANOVI;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        if (cursor.moveToFirst()) {
            do {
                MemberDB member = new MemberDB();
                member.setID(Integer.parseInt(cursor.getString(0)));
                member.setDate(cursor.getString(1));
                member.setNumber(cursor.getInt(2));
                // Adding contact to list
                contactList.add(member);
            } while (cursor.moveToNext());
        }
        return contactList;
    }

    public void deleteEntry(MemberDB member) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_CLANOVI, KEY_ID + " = ?",
                new String[] { String.valueOf(member.getID()) });
        db.close();
    }

}
